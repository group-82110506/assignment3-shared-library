def call(service_name) {
    pipeline {
        agent any

        environment {
            SSH_USERNAME = 'ubuntu'
            SSH_IP = 'ec2-54-71-131-230.us-west-2.compute.amazonaws.com'
        }

        stages {
            stage('Lint') {
                steps {
                    sh 'if [ -d .venv ]; then rm -rf .venv; fi'
                    sh 'python3 -m venv .venv'
                    sh """
                        . .venv/bin/activate
                        pip install pylint
                        python3 -m pylint --disable import-error --fail-under=5 ${service_name}/app/*.py
                    """
                }
            }

            stage('Dependency Vulnerability Scan') {
                steps {
                    script {
                        sh """
                            . .venv/bin/activate
                            pip install bandit
                            bandit -r ${service_name}/app
                        """
                    }
                }
            }

            stage('Build Docker Image') {
                steps {
                    withCredentials([string(credentialsId: 'DockerHub', variable: 'TOKEN')]) {
                        sh "docker login -u 'bwoo14' -p '$TOKEN' docker.io"
                        sh "docker build -t bwoo14/${service_name}:latest ./${service_name}/."
                        sh "docker push bwoo14/${service_name}:latest"
                    }
                }
            }

            stage('Deploy') {
                steps {
                    sshagent(credentials: ['AWS-VM-SSH']) {
                        sh """
                            ssh -tt -o StrictHostKeyChecking=no $SSH_USERNAME@$SSH_IP \
                            "cd /home/ubuntu/acit-3855_docker_containers/deployment && docker-compose pull ${service_name} && docker-compose up -d"
                        """
                    }
                }
            }
        }
    }
}
